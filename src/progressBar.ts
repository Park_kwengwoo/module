import { Option } from './common/app'
import css from './public/css/style.css'
import logo_1 from './public/img/default_1.png' // 기본 이미지 1
import logo_2 from './public/img/default_2.png' // 기본 이미지 2

interface ProgressBar {
	setup(): void
}
export default class ProgressBarImpl implements ProgressBar {
	option: Option

	private initial = 0

	protected progressBar = document.createElement('div')
	protected container = document.createElement('div')
	protected img = document.createElement('img') // 아이콘
	protected style = document.createElement('style') // 스타일 시트 지정

	constructor(option: Option) {
		this.option = option
		this.initial = this.option.barSet as number
	}

	// 초기 위치에 따른 바 이동
	get barSet() {
		return (this.progressBar.style.width = this.initial + '%')
	}

	// icon true 시 이미지 설정 후 반환
	private iconImpl() {
		if (this.option.icon) {
			// 이미지 설정
			this.img.setAttribute('class', 'img')

			// 이미지 url을 설정 안할 시 default 는 logo_1
			let imgUrl = this.option.imgUrl ?? logo_1
			this.img.src = imgUrl

			// 기본 이미지 선택
			if (imgUrl === 'logo1') this.img.src = logo_1
			else if (imgUrl === 'logo2') this.img.src = logo_2
		} else return this.img.remove
	}

	// css 셋팅
	private setStyle() {
		// 컨테이너
		this.container.setAttribute('class', 'container')

		// 프로그레스 바
		this.progressBar.setAttribute('class', 'progressbar')
		this.progressBar.style.backgroundColor = this.option.color as string
		this.set(this.initial)

		// 구현된 아이콘
		this.iconImpl()

		// 속성에 스타일 시트 적용
		this.style.innerText = css
		document.body.appendChild(this.style)
	}

	// 해당 아이디 값을 가진 노드에 container 부착
	setup(): void {
		// 스타일 셋팅
		this.setStyle()

		this.container.appendChild(this.progressBar)
		this.container.appendChild(this.img)

		// id 값이 없다면 body에 부착
		if (this.option.id) {
			const element = document.getElementById(`${this.option.id}`) as HTMLDivElement
			element.appendChild(this.container)
		} else document.body.insertAdjacentElement('afterbegin', this.container)
	}

	// bar 시작 지점 초기화 (1~100)
	set(setPoint: number) {
		if (setPoint > 100 || setPoint < 0) return (setPoint = 0)

		this.initial = setPoint
		this.barSet
	}

	// 100까지 바가 애니메이션으로 작동
	start() {
		const speed = this.option.speed ?? 200
		this.progressBar.style.transition = `width ${speed}ms ease`

		let buffer = setInterval(() => {
			if (this.initial == 100) clearInterval(buffer)
			else {
				this.initial++
				this.barSet
			}
		}, 10)
	}

	// 바 종료 시 비동기적으로 작동
	done() {
		this.set(100)
		setTimeout(() => {
			this.hide()
		}, 300)
	}

	// 원하는 양 만큼 바 이동 후 초기 위치 저장
	increase(amount: number) {
		if (amount < 0 || amount > 100) return
		else if (this.initial === undefined) this.initial = 0

		let endPoint = this.initial + amount
		// 바 이동이 100을 넘으면 원하는 위치를 100으로 반환
		if (endPoint > 100) endPoint = 100

		let buffer = setInterval(() => {
			if (this.initial > endPoint) clearInterval(buffer)
			else {
				this.initial++
				this.barSet
			}
		}, 10)
		this.initial = endPoint
	}

	// 프로그레스 바 보여주기 설정
	show() {
		return (this.container.style.display = 'table')
	}

	// 프로그레스 바 숨김 설정
	hide() {
		return (this.container.style.display = 'none')
	}
}
