import { ProgressBar } from '.'
import { Option } from './common/app'
import ProgressBarImpl from './progressBar'

export default class ScrollBar extends ProgressBarImpl implements ProgressBar {
	constructor(option: Option) {
		super(option)
	}

	// types='scroll'시 상단의 위치 값에 높이 차를 나눈 퍼센트 값 반환
	get scrolled() {
		// 페이지 상단의 위치값
		const winScroll = document.body.scrollTop || document.documentElement.scrollTop
		// 스크롤 시키지 않을 때 전체 높이에서 클라이언트가 스크롤 시켰을 때의 높이 차
		const preHeight = document.documentElement.scrollHeight - document.documentElement.clientHeight
		return (winScroll / preHeight) * 100
	}

	// 윈도우를 스크롤 시 프로그레스 바 작동
	private scrollEvent() {
		window.addEventListener('scroll', () => {
			this.progressBar.style.width = this.scrolled + '%'
			if (this.progressBar.style.width > '98%') this.img.style.display = 'none'
			else this.img.style.display = 'table-cell'
		})
	}

	// 0~100 까지 퍼센트로 원하는 스크롤 위치 지정 및 바 동작
	move(endPoint: number): void {
		const winScroll = document.body.scrollHeight || document.documentElement.scrollHeight
		const location = (endPoint * winScroll) / 100

		window.scrollTo({ top: location, behavior: 'smooth' })
	}

	setup(): void {
		this.container.style.top = '0'
		super.setup()
		this.scrollEvent()
	}
}
