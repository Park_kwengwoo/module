import { ProgressBar } from '.'
import { Option } from './common/app'
import ProgressBarImpl from './progressBar'

export default class LoadingBar extends ProgressBarImpl implements ProgressBar {
	constructor(option: Option) {
		super(option)
	}

	// 로딩 바 css로 변환
	private setLoadingStyle() {
		this.container.classList.replace('container', 'loadingBox')
		this.progressBar.classList.replace('progressbar', 'loading')
		this.img.classList.replace('img', 'loadingImg')

		// 바탕 색 투명, 외곽선 색 지정
		this.progressBar.style.backgroundColor = 'transparent'
		this.progressBar.style.borderColor = this.option.color as string
	}

	setup(): void {
		super.setup()
		this.setLoadingStyle()
	}
}
