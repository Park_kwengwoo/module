export type Option = {
	id?: string // 모듈에 부착할 노드 id
	barSet?: number // 초기 바 위치
	speed?: number // 바 스피드
	color?: string
	icon?: boolean
	imgUrl?: string // 아아콘 이미지
}
