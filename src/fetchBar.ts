import axios, { Axios, AxiosResponse } from 'axios'
import { ProgressBar } from '.'
import { Option } from './common/app'
import ProgressBarImpl from './progressBar'

// 요청 받는 응답에 따른 프로그레스 진행률
const percentage = (loaded: number, total: number) => Math.floor(loaded * 1.0) / total
export default class FetchBar extends ProgressBarImpl implements ProgressBar {
	constructor(option: Option) {
		super(option)
	}

	// axios 요청, 응답을 과정에 프로그레스 바를 설정
	progress(instance: Axios = axios) {
		let requestsCounter = 0

		// 요청 인터셉터 추가, 프로그레스 시작 시 바 설정
		const setupStartProgress = () => {
			instance.interceptors.request.use(config => {
				requestsCounter++
				this.start()
				return config
			})
		}

		// progressEvent 시 프로그레스 바 실행
		const setupUpdateProgress = () => {
			const update = (e: ProgressEvent) => this.set(percentage(e.loaded, e.total) * 100)

			instance.defaults.onDownloadProgress = update
			instance.defaults.onUploadProgress = update

			console.log(this.progressBar.style.width)
		}

		// 프로그레스가 끝날 시 프로그레스 바 종료
		const setupStopProgress = () => {
			const responseFunc = (response: AxiosResponse) => {
				if (--requestsCounter === 0) {
					this.done()
				}
			}
			// 오류 응답 처리
			const errorFunc = (error: Error) => {
				if (--requestsCounter === 0) {
					this.hide()
				}
				return Promise.reject(error)
			}

			instance.interceptors.response.use(responseFunc, errorFunc)
		}

		setupStartProgress()
		setupUpdateProgress()
		setupStopProgress()
	}

	setup(): void {
		super.setup()
	}
}
