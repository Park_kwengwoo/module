import { Option } from '@/common/app';
interface ProgressBar {
    setup(): void;
}
export default class ProgressBarImpl implements ProgressBar {
    option: Option;
    private initial;
    protected progressBar: HTMLDivElement;
    protected container: HTMLDivElement;
    protected img: HTMLImageElement;
    protected style: HTMLStyleElement;
    constructor(option: Option);
    get barSet(): string;
    private iconImpl;
    private setStyle;
    setup(): void;
    set(setPoint: number): 0 | undefined;
    start(): void;
    done(): void;
    increase(amount: number): void;
    show(): string;
    hide(): string;
}
export {};
