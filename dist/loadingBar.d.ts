import { ProgressBar } from '.';
import ProgressBarImpl from '@/progressBar';
import { Option } from '@/common/app';
export default class LoadingBar extends ProgressBarImpl implements ProgressBar {
    constructor(option: Option);
    private setLoadingStyle;
    setup(): void;
}
