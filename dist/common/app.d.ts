export declare type Option = {
    id?: string;
    barSet?: number;
    speed?: number;
    color?: string;
    icon?: boolean;
    imgUrl?: string;
};
