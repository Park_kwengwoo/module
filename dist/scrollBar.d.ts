import { ProgressBar } from '.';
import ProgressBarImpl from '@/progressBar';
import { Option } from '@/common/app';
export default class ScrollBar extends ProgressBarImpl implements ProgressBar {
    constructor(option: Option);
    get scrolled(): number;
    private scrollEvent;
    move(endPoint: number): void;
    setup(): void;
}
