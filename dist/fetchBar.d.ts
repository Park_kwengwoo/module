import { ProgressBar } from '.';
import { Option } from './common/app';
import ProgressBarImpl from '@/progressBar';
import { Axios } from 'axios';
export default class FetchBar extends ProgressBarImpl implements ProgressBar {
    constructor(option: Option);
    progress(instance?: Axios): void;
    setup(): void;
}
