'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  Object.defineProperty(Constructor, "prototype", {
    writable: false
  });
  return Constructor;
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

var img$1 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAmCAYAAAC29NkdAAAABHNCSVQICAgIfAhkiAAABhVJREFUWEfVmH9MU1cUx7+vP/hlEeSHDpCJOtjQgB2/toUhTBPUZEriQmiEKcMIGKOiLtkfukwSxj9GLRozwF8sIuCIZETc0D+U6X5ChcIyQVBRcW5GRQsFhLavO/dhHeW1iNa67CQvfc1995zPPfece8+9HBwUpVIV4uvruYvURPf1Dc6eOtWj22jku3S6gRyttuqGg+rBOaIgNnZVpkIxRa1SLfFKSopBWNgsdHbexKVL7Th4sEan1w/kNTVVlDliY0JA5h2ZTLqZDCi9vRWyyMiwKRZjJpPJY2TE+MbMmdOlrq4uIobh4RHcuvU37+bm0iWVSgctH+h0elNra6ceMD8CJEUaTXnDRAOwC0jeUZrN3Pns7JXeKtVSeHp6OOIIUd+GBg0OHaphHv9kIi9PAJhetnVrxhoG5yzp7x/E4sXZaGo6bpdjIg92nzt3MEShEHuu7Y8efHX0HLymuiM4yBcJ74ahUF2HBfODhf8Zqe+hvPoXYVyWd/ZrS3Jzv6SY7fjA3lTbBVyxYrO5tlZtU2nh3lOInBeMtss9uPhrJ4F6oOrgenyW/w38/Tzh7uaCW7cfIIK+0fUN4nf6rnh3pk1dlEyUUCftTrNNwJiYjKSkpKjzu3ZtsQv4YbISdWe1AuCGrMXC+02CqiTQ+GUF+K5qG7y83IX3ytL1iCTv2pK6uovIzy/O12gqdtpqtwOYnkfJsXfdupXOCr+nepub25GTU/ADASZNGjA2Nr2MvLcmMTEap09fw9WrD9HV1fvSYAMCFGBPaupbtDq4IC4u/QZl8uznAFzVUltbpNTrpcjMrHtpYOMVbdoUg7S0cKSk5NGaqZum1ZbR2mgtNqeYPGhubCwXvFdY+LPTAJkH8/JiMVEmiwDZAh0VNa+luHg7Dh9uxZEjbU4DVCpn4MCBZOzZU47KyvotGs1x0bIhALItTS6XpBgMfC39JqlUy47SIo0NG85Cq73rNECFQo4zZ1SoqqonyGNFZF9t4bAUGpxSmektkxm6OQ7ejMRsNp/ctu3jj9gOkpxchYEBg9MAmeL6+jRKwGvIzi64xHHcXMZhNuOR0SifzWKSYxUJwB21UPC8+WZp6Y5ZoaFzsXTpCafCMeX79ycjNNQTixatGyJAd4tBghSmnIuJWbWTGr4Y00B7Yzmam+9i48azTge0ZHJCQhZYBfSvmIto6cljgA0EmGhp8PHxIrcfwIkT7di3T+N0wKysSKxduwCrV3+Ojo7up/Yo1ITFm00xZQ7Haj4Wf4Px8W97qNWfQq1uQnV1h9MBx2ZyRcX3DyUSbtoTo19TlZMpJIlcPrKT5jyE5/nHubmpaWyLu3ChR/Cis4Ut1AsXBguZvHv3sZNkT0aP1mh0UQtJMhaATXdJyY7EqKhwZ3OJ9Nvbk60Aabq7aYsLCQjwe+WA9orXcYCjW9x/JXFxGaLqWgT405n14EceQOLiR48vpFPedBqvaeCKYIsfuS/Yil/y1bMBL55KxfDdU1ZQnNwXvSPzMSyPFcBfVPjhv6Awa6BAC8APWalxnbEcCcurXwyQaWq+lwMD7yliCwz0wpw5fnB3lwttQ0MGXL9+H3fu6GyOw0N2HxG+ZaI2hwAHDP64zedCJpOIFEdGzoRUKq7aWlp6bAKaTGa8LlHDTdb3Yh787Uc1eMMDq85XrvHo7XMhEDGgv78CwcHT4OrKli/QdmVET89D3LtHZ3MbYjLxCJxuwKwg60FJKIzeeT/v2VNsK4ubm3vQ3/8YEokY8HnjkYoRKvNdERUlPkRNKovZUXP8OkiXQWhr+5OmWPq8PKLvjUYTwsNfexqzlg/0+kGqaMSH+HE7SfpAScl2D1s7SW/vIBobbzgMGBERiKAgofS0EraT5OYWmKiCGY2VJzJ+qzNHR88DK/dtCZtmqnodgvTxsX3HM3ouuQyqYKyYRAs1s86KhVdxJraMdPR2oUb4O/6eRgRIJReoPqQgDqcyfCXFo78oJh1y4ZPOLObYXWJpaQ0Vx+2s1BPsPhPQYpx1YMI6OUts2Zg0oAVqoqsxR8HZ+Xu8jv83IBWs39KUpowZVSuNSOmop+z1n4y9fwBTIb5F3MzUggAAAABJRU5ErkJggg==";

var img = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKAAAACgCAYAAACLz2ctAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyFpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQyIDc5LjE2MDkyNCwgMjAxNy8wNy8xMy0wMTowNjozOSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpFRjM0RDE3RUE1MkYxMUU5QUUxRTgxMjYwNzM1MjIyRiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpFRjM0RDE3RkE1MkYxMUU5QUUxRTgxMjYwNzM1MjIyRiI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkVGMzREMTdDQTUyRjExRTlBRTFFODEyNjA3MzUyMjJGIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkVGMzREMTdEQTUyRjExRTlBRTFFODEyNjA3MzUyMjJGIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+6x5olQAAGL5JREFUeNrsnQd4VGXThp9NJYWQRgg9ASIElKJ05aNXFREIPx8oIKig2LAriiioCPjTRFGKovQWitiQ0KQXQ4kISSCFQArpve438549m6UHlISEea5rcracnHqfeWfetgaj0QiRqKxkJZdAJACKBECRSAAUCYAikQAoEgBFIgFQJACKRAKgSAAUiQRAkQAoEgmAIgFQJBIARQKgSCQAigRAkUgAFAmAIpEAKBIARSIBUCQAikQCoEgAFIkEQJEAKBIJgCIBUCQSAEUCoEgAFIkEQJEAKBIJgCIBUCQSAEUCoEgkAIoEQJFIABQJgCKRACgSAEUiAVAkAIpEAqBIABSJBECRACgS3bRsyvoAmjf/P7kLZajg4JXiAUXiAcuL7Mi+IutA5s0PMNkusg1k7mRVyFzIXE1LJ5M5klUicyazJ7MlszYtDZfto8BkeWQ5ZOlkGWSpZIkmiyOLIYslixKMKiaAtiaovMiqktUhe5lLbYt1Opjs3TI8zkgThMfIjpAdJDsuaJUfAOuZIGpBVpPM0wRck6utbCB/lZqagUqV7JGSkgE3t8qwtrZCUZGRrAgFBYUoLCw0v+el0VhEBvXetBXztq4mXpf+qvWsrAy0Hi+tYGPDZgNbWxu1T/6MVNdkHSw2cYrsD7LfyTbb2dmmC2p3IICUgEykxeumYvIaMBgJKAarABkZWbhw4SLmzHkbvXo9iI0bt2P16t8IBGtUruwEFxdHMmc4ONjD3t5OLZ2cHODo6EDLSvTeTgFkZ6dBZGtrZQaJYSzeVyHy8wuUZWRkIzk5HRcvpqn95+bmIi4uCYmJKerzpKQ05OUVqO04Ozuo/RKwDenQ2UbxOeTl5a/gMIFAXCHIXeZQjNrjXhbwfUOLZ/g13+j09ExaFirPY21tTZDYKlCqVHGGu7srLZ3g5eWOTp1aoX//LpdjepVQ7vbLaMwkGBNw7lw8QkLOUEYZhkOHTiM8/Lw6B1fXymppocNkv5AtoPOLuBMAOHBgyd0HIMFnawrylde55566aNKkPmrUqEqezBEeHm7w9nZX0Hl7u5FXcbzhNpOS8gniXKSlseUgKioFPXvWV98tWXKCPFchebEsZGbmIyeHPVs+srMLyDsVKq9XWFh8HaytDcpsba2pqLdR6yxc2AdhYSmYOnU/atd2Ud7Vw8MZDRp4omZNFzp2ZzrOAvJ+DGU0du/+E4GBe3D4cKh6mNzdq1gW+VwkryKbSd+dEABLH0AXU1aJ8+cTcPToKlSv7nnN9S9ezENkZAoVv2kEVhrFfjl0kzORkJCNrKx89T49PU9BlpdXRJ/lITY2A9HRL+Dbb49j/PifyXt6qliOweJYTi9++T2DYbCggy8JXxeOHwsKisijJZKHG03HmoHu3b9D3bpe6nM2Bpf/1dHRloB0ogfJGx061Ee7djUISiMdYwTWrduOpUuD6PgT6YHyUMW/hev+juxjAjFcACxdCLn65KG0tEwqVlti0aKJ5u8CA09j164o8mo5VLylU6yVo17n5nJcpiUSdnbWykPZ2VkpL8VAccLAQMXFZaFz57pYsKA3unZdTv+bTZ7V7rIko4QXiOBiz7dqVT/y1O5o23YxgVUZVuYaVA1cTnD42FJTc9X+bGys0aJFTQwa1ByPPFKPYtM0/PjjdsyevR7HjoWhWjUPOm7zMWWRfUH2NoFovJsALMuK6HX8x8XFCT/9tAsBAW+Qp0tWX3zxxWHMmBGEPXtiyGtkqM+qV3eCr68rGjXyQMOGHvDxqUJFnzOqVnVUcDGEOpQM7KOPNiAPmEaeK4H2YW/yajcHnw4Yw8Xez9vbicICe1UkF2/PqIw9KO+fj8ff3xN+fm7krZPw6qsbCNp5mDgxBA8+2Bc7dizCihUf0LFXRUREjIp/TfWUb3JVDiUsQ++mJKTMAAwOXjmDFov5taenGz2JJ1SRx5o7twcVc7XUDeYbynDpRaRWrWLE9bJmR0cbtGlTnW52lCom/3myAeXVWB4eDsoTX09aFZCRkhB7NG3qRefnQDHkQfKIc/Dii9upeO6KrVu/Ia8/XlUnRUZe0B+MezlkJQhXkVUXAG9vEexBi+6qJpduwKRJY1WWy2Ivt3z5Y0hMzFaxHhevJRUXgc2bV4ObmwM2buSizskM9i3ip4p2PhbNEzurBKak4geAY85GjdyV11658ijFiXPJI+6lbL43JSnL8M47w8n7pxDkqfq/BZi84UgB8PZpKlkNLoL8/ethyJDel3zZrl1NulmPKQBLCiE7SV63d+/6qhg+cOA8ZZ8O/9j7cbGekJBlArCyyqJvVpysMMhcPDPEM2bsJe+4EGvWnMGbb46iuPB7Kqrvo3gzWmXl0CrkFxKE7BFdBMB/1/txm2xn3fs9+2x/FbRfru7dfQnCfoiPz6Ks9sYQcjHNiUDHjnXIs8Qqb8g3/Z+KPRgnQSwfHxe1D4Ph1rbLHpHPo0ULb/X6qacCMXBgICVP1SjRmUbhx1uqTpQ9oklDTd5wlAD473o/X24ya9jQB8OGPXLNFRnCtWv7K4/GGTEH+tcS1+s1aOCmkpPNm8NV7PXPil8dQANtWyt2a9asrDLtfyqudOd4kkHctSsazZp9ia+/Pobhwx/DiRNL0br1vQgNjdKPn5v6FhCEh8jmkY0nG0jWnMxOALw57zeQFv35dUTEeTz99OOqovZ66tSpDgIDB6iiLzIy9aoQskOKj89E164+Kkvdvj2SEhinf+WY2QNyHaPmAavAwcHGol351sVwcXHr7++hEpbXXtuCwYM3UGLihnXrPseUKS+qmoHY2EQ9SXmAbDTZZLLVZH+S5RKEv5B9SNaHzKs8AViq9YAE3zJa/Jdf04VSFz84eAXd0EolzJzj0K/fWlUce3k5mmM0/RxiYuLJ8w1X3vKJJ1agTp3q6iZrHQqglpbGnkx7baXq9fSOB3rFtL7kFhYvLyds2zZEZcOdOi0zVftYXREv3vKNMGjHwtVGnPnPmtUTPXvWRVTUOXz00ULs3HkEKSnpqhKb25u5DpFfXyMU2Eu2H1pXtb30gF+4U+sBSw1Agu//aTGOLxhfSK4D45gnIKD7TW3nzJkUzJx5kDLRPDNIessGv37//Qfx669n8dNP4eY6O47Z2PQmN469+HP9O32ptWwUmatRCgq0dVNTc9T2Q0PHwNnZjjLab3D6dJIq4rUeMzAfh1YXqbeyWKnimz/XWlwMZtgss2zLW8D/z3WOnEy99FJLgk/rZMOx8r59x1WSEh+fSOFInHrguHNGbm6eqs5xdna8aiwNrWcOw7iDjYDMuRsB5H5yLbOycine8cO4cU+gfftmZXbiDCLHYZYg6s1rDB6DyEW+3m7MdX9duvio4nfLlrP0AKWa6gdzVPszNwnyupz4pKXlqfe8Xf6M25/5/7XtGs2QqxjIyqCA1SDVlpUqWYNL+FOnEtGjhy/mz3+YMucr28OTk9MUkMePh5GF4q+/zuDs2Rjaf4YKa7iHkEWzn6XWQOsqtik4eOX5uwVAtaOcnDwcPbpSFSMVVQwZg8fQcRt1SkquWuowcjHOkOodI9jD8jr8nqHlpQ5tcHCsqvrZvn2oagq8kU6disCxY6E4cuRvus6nyFNHqqyawxwGkiG/TN+TPUcgZlV0ANnt2/MTuWXLPNVVSXR1scdlr6pDyE2KbdrUuKU6zZMnz+LQob9US9Off/5NIUyMKvbd3FwsvWMY2eME4YmKDGA8Lapyb+a1a6fj/vv9hbTbIGMhObKiPBhsXa/4jlsQD+w7Qd50P7YF7SdPeRo1a3rpMenfZH0IwrMVtRrmuFZXl6OeStHtAjATWREzkXb8GWRHzkNu3CbkJe2CMecM7G3y0OGhe/H+e6MQFPQNPvz4Q5yPy9VrERpBG/BVqirNLvncG7gLB8cC4G30KHZV4VB3LLLOTEcmmcHWHdaVqlFZZw8rm8r0vRe994KDR0OMHd0O9gjA6+8shU8d1drXk0qqxuQF/6qIHpBdvAqGo6PjhJTbDKFzo8/g0nQhbJzqwViQTfC5kKcrREFWKHITfkfyyZnID3kGTw86jM4dvJGbZ64a6lVRi+AcvQ6Mm+DKWhcupOLbb7dSPHpIvY+PT8OiRb9j3bpDV6y7a9dpPPvsIowY8SXmz99u/nzLlhDExWVc6eoPh+Ls2QTKYCPpYUu94vsjR06r72+37Dw6ovK9X9LyPyjMPgtjfrIC0apSLVg71kdmnhulhSno9ZARKelmADtU1CKYx/WqFhAeH1GW2rMnFL16TUV6OleBVcLDD3dAaGgsTp8OURnikCF9sXjxKFWpO3t2EF5+eRq0PqOO9Plq/PLLMQL3Jbz22gqEh0chLGwWqlfXgv5hwxbghx/WU8b5BT76aCM2bfoDf/01G35+3ur7V15ZjlmzvsGGDZ/D17fq7fcwVAQ7+U2ArVt7FQ8W5V5QcaKxKF+rAM+2hqs9ZddW3D8kl//Fq6J6wOZaEpKLe+6pU6YATpgQSPAlUjY4E6tWvY3Nm3fA0dGOPNeXGD06AMuWrUNERCISEtIIvhlo1aotQkLmICZmDkE1jrzkGixfvg+ffRaArKy/sXr1QbXd6OhEgu8XtG7djrJ+Hwwa1AYFBVG0vf3q+5SULAL6F9Sv3xZ9+95fquds59kNlZvMgrP/5wpIR5+xcKg1lGKiQcixbQFDkdmTp1VUAO/TimCoUXBlqaSkTHh61kbHjn4ICGjFtwf33+9DVhudOnEyWESAZhNonDcZyFu9gsaNeZCRK95/vy/9X29MmrQRzZrVQeXKzbBypVZsb9vGYW48XnhB9TRDnz5N1ffLl2sA7t0bTl4nkqDuWmbnbu1QG7aubWBf7TECcDg59adQ6PggUGju/pVVUQGsxX94DEjdujXKFED2djxbAisujmM0W3N3K4aTgeQWg4sX2Su4UPF6acjg51eLMvloBeSwYR2pSD9I3jKdPOop2NnVQ79+mndzcXHA8OH/walTRxAbm0oAhoInfdCgv3OUn5NMjsEcjeVXVADVGXITHM8gUJa6sgOJ4Yr33ErQowcP0biAKVM2m78JCYnBwoUb0aXLvep9QEBLdRnffHMV1q8/jCef7ERer7h3z7Bh7cFzIr399hoqnvfgkUf+A2/vKncUgJc1RpTqqLzSTEIMVznZMlFExEUkJmo9lLTu79E4dy7J5AHZ60VQ4hCDAQNaonPnbnjnnenYv/+MSjS++upn/i98/vlgtf5DD/lRSOGH777bxAUcRo588JJ9tWrli5Ytm1Pysl69nzv3yTuw4sZQ2tyVCYAc3DpwUxx3sOQmoFsRV+Hs2HGEstZIgiiVPKqtmlHhgQcalzi2fPjh5pSNa3Mfubs7k9cagDZtGqn3Xbr448iRgfS51pl18+bX8dxzVSmR2IX8/FzaT0NKJIZTkqHti4vqzz4biHHjvlbz1bRv73fF/j799HFKbs6jXbtmtE7Tf3QRuUtWSEgY4uOTFTQ8kKtJkwZqPElJVJSfjOzoRZQdu8Jg7YRK3v6wUW3C5m5c1qWKfim2Bf/I9z4mJgGTJ4+lGzLgprcxZ84KAuEnREXFmvrbWSuPyhMXcQtL27ZNqSgcQftq+K8ff3JyBjIzc1GrlkeZeIrVq7dg/vx15JnDVV0qT7LE4nPna+DvXx/PPNOfMu/r9680FuUi7c8hKMyOItQc4d60Hr5b1hbvfvgrvDxVz/Qfg4NXPloRY0DuEAkPDxf89tuem3OdaRnkOcZi/Pg5lJ1mqVkFqlZ1V/WJHh6u6j3PisXeoXPnZ7Bgwbp//eDd3JzLDL6xYz+lZGYCoqNj1Xmz1+NzZ/Py0q4FfzdixARa95Prexwre8qAHyVPSMkXd1ywaw4n1wYoKjL3US3VkyzNIngj2RTuubt16wGKqY5TsXfjYiMrK0fBx16vfv3alvPymXsWswfgz7iLEU9u9Nxzk9SUbSNHPobyrmefnYTly39WdadcMX61c9d7X/M6S5ZsVtPFzZ8/4ZrbtPfuj6KCdBhsXADboWjSLAVVnM0lb8PSPL9S84Dk1k9yVZnmBV0xadL8Ev3fW2/NUqPDPD1d1Q3Qb0JCQrKKA8+cOac6ueqTRvKycWM/vPTSZ6pjZnnWokXrCahN6sHTz5uXPE9heHi0ui7cK5pDETY+d1/fWgTsj5Spr7+2FyTwHH1e1OoByQf51KwEN3dz960qFRJAk6bwH/ZS27YdpJhu+XVXPnjwBH74YbNKWPji8sXPzs5DWFgU7rvPD2PGDMLQoX1UYsIj7DgO1G8SF8uTJ88vt/Dx5JdTp34HPz8fBRefFy+5Cz6P/Rgx4jHyjgNQu7a36vXMo/R4Hb5O9erVxbRpi9XkmSXRhdhkyw4iRaV5nqU6Qyp5wd8oGeEArb+fXx188MFXap69UaMev+rorqVLf6Y4R5vkURt0XkgZdALeffdpvPrqE+b12BOMGfMxTp48Ax8frZKbZ0vdvTuYMsZwNfdgedP69UEKIH74dM/OoHXr3hazZ71JcZ+bWi+XvP97E77Et4s2EKy1qUjmOkxbNYtrYGAQXdt+N9zXK69Mt5ypa1dF9oCs58gO8At+esePn4uuXUdj4sR5qtu4pbgruZOTo3nIIg+4GT68r4KvkGKf1LRMZQzzkiUfqzEPXDypE7PSYsU//ggulx6QrwUnGfrMDvHxSaqaadmyTxV8+rnbEzjTpr6Cbt1aqzhZH07KM8tynH098YPbvfsYVdLwtTNpcYUGkLwgd83naSbO8sXiiSnj4hIxb94a9Os3Dv37v4a1a7dSjKPFdvpYXZ63mb3l4MFad7W01EzzNtMzstV2uFfL+ZgEkzc1qKI4MTG5XALIwy35+DWgDOoaDR7cU1UZJ6cUdwFLStbmPx8ypA8yM7OhT1fMHpP/51qaPXs5QTtGeVVvb/PkoAvo/nxfYYtgCwhPUFHcGloX8IF8sXjmUBYPL3zxxSmoW7e6mhSI4zvmiYdLcmdWhlDzcMVFtt6/kCckzy8o7mvIxcrff0dS1r3fPPa2ePD5pWb5ufbayjzu+PLP9Fnzr3xtOf7XcoD75QPer/5e99q8LZ60yTIq4eN3dnYyvS4y1xfroYsjnTsDy+vxRxwTXj4CLicnF+vWBVGCEkily0m6xt7mrJo0ne7LG6XNQpnNkk8ne5EWAQQiP378e108XUcXhogtMzPnEsg4wYiPP0cXLgT+/r7macl5fK2bq7NaZ9++Yypb1irXjWqwNo8ECwraj0t/muHSn2kojj8vnRVBf39tmPTvDOaldj+v/n/6voqnBC7elj5Dgw4xA6hVtGtVLdx+vnPnYTz1VF+VhHD1FIsfUtbePUdRVKhNmsTwcZv7xYupeP75T1S9IffD3L37KE6cCFPXyNfX3CGEh8m9TfejTEaoG8q6bbZ16+Jkgi4SdxMZAW3eQHObFtf68w3iCmlrespXr5qGxo3rXbKd8e99ga+/WgO/e+qo9l2+gXdCu7OlBzO9KsF6RrNn0rN6Fs8m8emnL1+RWGzZsg8jR06Eh0cV9aBqM0BoJQFX3HNrCYPJtQ+Xzdq/iOwNgi+prK7LHQWgBYjcnYS7hnMxPZkvHhfT/LMNXF3AHnL06IFocX8jpKakq2Jl06YdqpsX3yxtvmY1hx8Prkk31W0VWcS9Boul5We+d2I8qFcvcYyXkJCE/1K816tne3VNeDb+BQsC1TrcIsKw3eDhO0O2k2wu/c+hu3aS8usBeBmM3NvzAa2yVauO4MpX7tDATzTHNVxs+fhoM9pykWxxA6bTRS5xXEP70kG8WbOyMGsLoHV3c72l5br6a34AI6BNXN5PfwD5GuTn5yMy4oLqQMDXg4viOnW81Y/xMHycrJlm7uL5YHg+Hm7Z4DAn31TFwnPDZBZn2wLgjaDgtPdndbdMHRD0WEpLULSgXZ8snG+A6Zy20IXugXIsOnceSPITWQttNi8bc/zIkPFp8jXR76EFfFwX1aYkv0FyN8+SX9Lih39ZqLeW7WpFq/YbcEXmn9ji1wyjhefbaapvLNeic+df4xxGFqZ59nzT7+AVmTNz7dyLr4sp1GhT1j+Ac8dnwRZPeUlWYwi7kX1CgLUuKLjusM75JvgKS7jtO10MUntTldUALcG4ZmsZP3jPk4WUl3MvT78XvJWfbGhzJnc0JSk1TbHNCVN8w818R1DxxIOIeWbZLqbSgM/d3xQvhpvivSCywPJ2Yjbl8GYsNdndqCCTVRhZQSS6m7Ng0d0t8YAiAVAkAIpEAqBIABSJBECRACgSCYAiAVAkEgBFAqBIJACKBECRSAAUCYAikQAoEgBFIgFQJACKRAKgSAAUiQRAkQAoEgmAIgFQJBIARQKgSCQAigRAkUgAFAmAIpEAKBIARQKgSCQAigRAkUgAFAmAIpEAKBIARSIBUCQAikQCoEgAFIkEQJEAKBIJgCIBUCQSAEUCoEgkAIoEQJFIABSVM/1PgAEAPL+aBTQ/gH4AAAAASUVORK5CYII=";

var css = ".container {\n\tleft: 0;\n\twidth: 100%;\n\tposition: fixed;\n\tdisplay: table;\n\ttable-layout: fixed;\n}\n.progressbar {\n\tbackground-color: cadetblue;\n\theight: 5px;\n\tborder-radius: 4px;\n\toverflow: hidden;\n\tdisplay: table-cell;\n\twidth: 0%;\n\tfloat: left;\n}\n.progressbar.progress {\n\theight: 20px;\n}\n\n.img {\n\tfloat: left;\n\theight: 20px;\n\tdisplay: table-cell;\n}\n\n.loadingBox {\n\tleft: 0;\n\twidth: 100%;\n\theight: 0.4em;\n\tposition: fixed;\n\tdisplay: flex;\n\ttable-layout: fixed;\n\tjustify-content: center;\n\talign-items: center;\n}\n\n.loading {\n\tposition: fixed;\n\tmargin: auto;\n\theight: 40px;\n\twidth: 40px;\n\tborder-radius: 50%;\n\tborder: dashed 6px cadetblue;\n\t-webkit-animation-name: spin;\n\t-webkit-animation-duration: 4s;\n\t-webkit-animation-iteration-count: infinite;\n\t-webkit-animation-timing-function: linear;\n}\n\n.loadingImg {\n\theight: 25px;\n\tposition: fixed;\n}\n\n@-webkit-keyframes spin {\n\tfrom {\n\t\t-webkit-transform: rotate(0deg);\n\t}\n\tto {\n\t\t-webkit-transform: rotate(360deg);\n\t}\n}\n";

var ProgressBarImpl = /*#__PURE__*/function () {
  // 아이콘
  // 스타일 시트 지정
  function ProgressBarImpl(option) {
    _classCallCheck(this, ProgressBarImpl);

    _defineProperty(this, "initial", 0);

    _defineProperty(this, "progressBar", document.createElement('div'));

    _defineProperty(this, "container", document.createElement('div'));

    _defineProperty(this, "img", document.createElement('img'));

    _defineProperty(this, "style", document.createElement('style'));

    this.option = option;
    this.initial = this.option.barSet;
  } // 초기 위치에 따른 바 이동


  _createClass(ProgressBarImpl, [{
    key: "barSet",
    get: function get() {
      return this.progressBar.style.width = this.initial + '%';
    } // icon true 시 이미지 설정 후 반환

  }, {
    key: "iconImpl",
    value: function iconImpl() {
      if (this.option.icon) {
        var _this$option$imgUrl;

        // 이미지 설정
        this.img.setAttribute('class', 'img'); // 이미지 url을 설정 안할 시 default 는 logo_1

        var imgUrl = (_this$option$imgUrl = this.option.imgUrl) !== null && _this$option$imgUrl !== void 0 ? _this$option$imgUrl : img$1;
        this.img.src = imgUrl; // 기본 이미지 선택

        if (imgUrl === 'logo1') this.img.src = img$1;else if (imgUrl === 'logo2') this.img.src = img;
      } else return this.img.remove;
    } // css 셋팅

  }, {
    key: "setStyle",
    value: function setStyle() {
      // 컨테이너
      this.container.setAttribute('class', 'container'); // 프로그레스 바

      this.progressBar.setAttribute('class', 'progressbar');
      this.progressBar.style.backgroundColor = this.option.color;
      this.set(this.initial); // 구현된 아이콘

      this.iconImpl(); // 속성에 스타일 시트 적용

      this.style.innerText = css;
      document.body.appendChild(this.style);
    } // 해당 아이디 값을 가진 노드에 container 부착

  }, {
    key: "setup",
    value: function setup() {
      // 스타일 셋팅
      this.setStyle();
      this.container.appendChild(this.progressBar);
      this.container.appendChild(this.img); // id 값이 없다면 body에 부착

      if (this.option.id) {
        var element = document.getElementById("".concat(this.option.id));
        element.appendChild(this.container);
      } else document.body.insertAdjacentElement('afterbegin', this.container);
    } // bar 시작 지점 초기화 (1~100)

  }, {
    key: "set",
    value: function set(setPoint) {
      if (setPoint > 100 || setPoint < 0) return setPoint = 0;
      this.initial = setPoint;
      this.barSet;
    } // 100까지 바가 애니메이션으로 작동

  }, {
    key: "start",
    value: function start() {
      var _this$option$speed,
          _this = this;

      var speed = (_this$option$speed = this.option.speed) !== null && _this$option$speed !== void 0 ? _this$option$speed : 200;
      this.progressBar.style.transition = "width ".concat(speed, "ms ease");
      setInterval(function () {
        if (_this.initial == 100) clearInterval();else {
          _this.initial++;
          _this.barSet;
        }
      }, 10);
    } // 바 종료 시 비동기적으로 작동

  }, {
    key: "done",
    value: function done() {
      var _this2 = this;

      this.set(100);
      setTimeout(function () {
        _this2.hide();
      }, 300);
    } // 원하는 양 만큼 바 이동 후 초기 위치 저장

  }, {
    key: "increase",
    value: function increase(amount) {
      var _this3 = this;

      if (amount < 0 || amount > 100) return;else if (this.initial === undefined) this.initial = 0;
      var endPoint = this.initial + amount; // 바 이동이 100을 넘으면 원하는 위치를 100으로 반환

      if (endPoint > 100) endPoint = 100;
      var buffer = setInterval(function () {
        if (_this3.initial > endPoint) clearInterval(buffer);else {
          _this3.initial++;
          _this3.barSet;
        }
      }, 10);
      this.initial = endPoint;
    } // 프로그레스 바 보여주기 설정

  }, {
    key: "show",
    value: function show() {
      return this.container.style.display = 'table';
    } // 프로그레스 바 숨김 설정

  }, {
    key: "hide",
    value: function hide() {
      return this.container.style.display = 'none';
    }
  }]);

  return ProgressBarImpl;
}();

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _superPropBase(object, property) {
  while (!Object.prototype.hasOwnProperty.call(object, property)) {
    object = _getPrototypeOf(object);
    if (object === null) break;
  }

  return object;
}

function _get() {
  if (typeof Reflect !== "undefined" && Reflect.get) {
    _get = Reflect.get;
  } else {
    _get = function _get(target, property, receiver) {
      var base = _superPropBase(target, property);
      if (!base) return;
      var desc = Object.getOwnPropertyDescriptor(base, property);

      if (desc.get) {
        return desc.get.call(arguments.length < 3 ? target : receiver);
      }

      return desc.value;
    };
  }

  return _get.apply(this, arguments);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  Object.defineProperty(subClass, "prototype", {
    writable: false
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _typeof(obj) {
  "@babel/helpers - typeof";

  return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) {
    return typeof obj;
  } : function (obj) {
    return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
  }, _typeof(obj);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  } else if (call !== void 0) {
    throw new TypeError("Derived constructors may only return object or undefined");
  }

  return _assertThisInitialized(self);
}

function _createSuper$2(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$2(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _isNativeReflectConstruct$2() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

var ScrollBar = /*#__PURE__*/function (_ProgressBarImpl) {
  _inherits(ScrollBar, _ProgressBarImpl);

  var _super = _createSuper$2(ScrollBar);

  function ScrollBar(option) {
    _classCallCheck(this, ScrollBar);

    return _super.call(this, option);
  } // types='scroll'시 상단의 위치 값에 높이 차를 나눈 퍼센트 값 반환


  _createClass(ScrollBar, [{
    key: "scrolled",
    get: function get() {
      // 페이지 상단의 위치값
      var winScroll = document.body.scrollTop || document.documentElement.scrollTop; // 스크롤 시키지 않을 때 전체 높이에서 클라이언트가 스크롤 시켰을 때의 높이 차

      var preHeight = document.documentElement.scrollHeight - document.documentElement.clientHeight;
      return winScroll / preHeight * 100;
    } // 윈도우를 스크롤 시 프로그레스 바 작동

  }, {
    key: "scrollEvent",
    value: function scrollEvent() {
      var _this = this;

      window.addEventListener('scroll', function () {
        _this.progressBar.style.width = _this.scrolled + '%';
        if (_this.progressBar.style.width > '98%') _this.img.style.display = 'none';else _this.img.style.display = 'table-cell';
      });
    } // 0~100 까지 퍼센트로 원하는 스크롤 위치 지정 및 바 동작

  }, {
    key: "move",
    value: function move(endPoint) {
      var winScroll = document.body.scrollHeight || document.documentElement.scrollHeight;
      var location = endPoint * winScroll / 100;
      window.scrollTo({
        top: location,
        behavior: 'smooth'
      });
    }
  }, {
    key: "setup",
    value: function setup() {
      this.container.style.top = '0';

      _get(_getPrototypeOf(ScrollBar.prototype), "setup", this).call(this);

      this.scrollEvent();
    }
  }]);

  return ScrollBar;
}(ProgressBarImpl);

function _createSuper$1(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$1(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _isNativeReflectConstruct$1() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

var LoadingBar = /*#__PURE__*/function (_ProgressBarImpl) {
  _inherits(LoadingBar, _ProgressBarImpl);

  var _super = _createSuper$1(LoadingBar);

  function LoadingBar(option) {
    _classCallCheck(this, LoadingBar);

    return _super.call(this, option);
  } // 로딩 바 css로 변환


  _createClass(LoadingBar, [{
    key: "setLoadingStyle",
    value: function setLoadingStyle() {
      this.container.classList.replace('container', 'loadingBox');
      this.progressBar.classList.replace('progressbar', 'loading');
      this.img.classList.replace('img', 'loadingImg'); // 바탕 색 투명, 외곽선 색 지정

      this.progressBar.style.backgroundColor = 'transparent';
      this.progressBar.style.borderColor = this.option.color;
    }
  }, {
    key: "setup",
    value: function setup() {
      _get(_getPrototypeOf(LoadingBar.prototype), "setup", this).call(this);

      this.setLoadingStyle();
    }
  }]);

  return LoadingBar;
}(ProgressBarImpl);

var bind = function bind(fn, thisArg) {
  return function wrap() {
    var args = new Array(arguments.length);
    for (var i = 0; i < args.length; i++) {
      args[i] = arguments[i];
    }
    return fn.apply(thisArg, args);
  };
};

// utils is a library of generic helper functions non-specific to axios

var toString = Object.prototype.toString;

/**
 * Determine if a value is an Array
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Array, otherwise false
 */
function isArray(val) {
  return Array.isArray(val);
}

/**
 * Determine if a value is undefined
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if the value is undefined, otherwise false
 */
function isUndefined(val) {
  return typeof val === 'undefined';
}

/**
 * Determine if a value is a Buffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Buffer, otherwise false
 */
function isBuffer(val) {
  return val !== null && !isUndefined(val) && val.constructor !== null && !isUndefined(val.constructor)
    && typeof val.constructor.isBuffer === 'function' && val.constructor.isBuffer(val);
}

/**
 * Determine if a value is an ArrayBuffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an ArrayBuffer, otherwise false
 */
function isArrayBuffer(val) {
  return toString.call(val) === '[object ArrayBuffer]';
}

/**
 * Determine if a value is a FormData
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an FormData, otherwise false
 */
function isFormData(val) {
  return toString.call(val) === '[object FormData]';
}

/**
 * Determine if a value is a view on an ArrayBuffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a view on an ArrayBuffer, otherwise false
 */
function isArrayBufferView(val) {
  var result;
  if ((typeof ArrayBuffer !== 'undefined') && (ArrayBuffer.isView)) {
    result = ArrayBuffer.isView(val);
  } else {
    result = (val) && (val.buffer) && (isArrayBuffer(val.buffer));
  }
  return result;
}

/**
 * Determine if a value is a String
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a String, otherwise false
 */
function isString(val) {
  return typeof val === 'string';
}

/**
 * Determine if a value is a Number
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Number, otherwise false
 */
function isNumber(val) {
  return typeof val === 'number';
}

/**
 * Determine if a value is an Object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Object, otherwise false
 */
function isObject(val) {
  return val !== null && typeof val === 'object';
}

/**
 * Determine if a value is a plain Object
 *
 * @param {Object} val The value to test
 * @return {boolean} True if value is a plain Object, otherwise false
 */
function isPlainObject(val) {
  if (toString.call(val) !== '[object Object]') {
    return false;
  }

  var prototype = Object.getPrototypeOf(val);
  return prototype === null || prototype === Object.prototype;
}

/**
 * Determine if a value is a Date
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Date, otherwise false
 */
function isDate(val) {
  return toString.call(val) === '[object Date]';
}

/**
 * Determine if a value is a File
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a File, otherwise false
 */
function isFile(val) {
  return toString.call(val) === '[object File]';
}

/**
 * Determine if a value is a Blob
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Blob, otherwise false
 */
function isBlob(val) {
  return toString.call(val) === '[object Blob]';
}

/**
 * Determine if a value is a Function
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Function, otherwise false
 */
function isFunction(val) {
  return toString.call(val) === '[object Function]';
}

/**
 * Determine if a value is a Stream
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Stream, otherwise false
 */
function isStream(val) {
  return isObject(val) && isFunction(val.pipe);
}

/**
 * Determine if a value is a URLSearchParams object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a URLSearchParams object, otherwise false
 */
function isURLSearchParams(val) {
  return toString.call(val) === '[object URLSearchParams]';
}

/**
 * Trim excess whitespace off the beginning and end of a string
 *
 * @param {String} str The String to trim
 * @returns {String} The String freed of excess whitespace
 */
function trim(str) {
  return str.trim ? str.trim() : str.replace(/^\s+|\s+$/g, '');
}

/**
 * Determine if we're running in a standard browser environment
 *
 * This allows axios to run in a web worker, and react-native.
 * Both environments support XMLHttpRequest, but not fully standard globals.
 *
 * web workers:
 *  typeof window -> undefined
 *  typeof document -> undefined
 *
 * react-native:
 *  navigator.product -> 'ReactNative'
 * nativescript
 *  navigator.product -> 'NativeScript' or 'NS'
 */
function isStandardBrowserEnv() {
  if (typeof navigator !== 'undefined' && (navigator.product === 'ReactNative' ||
                                           navigator.product === 'NativeScript' ||
                                           navigator.product === 'NS')) {
    return false;
  }
  return (
    typeof window !== 'undefined' &&
    typeof document !== 'undefined'
  );
}

/**
 * Iterate over an Array or an Object invoking a function for each item.
 *
 * If `obj` is an Array callback will be called passing
 * the value, index, and complete array for each item.
 *
 * If 'obj' is an Object callback will be called passing
 * the value, key, and complete object for each property.
 *
 * @param {Object|Array} obj The object to iterate
 * @param {Function} fn The callback to invoke for each item
 */
function forEach(obj, fn) {
  // Don't bother if no value provided
  if (obj === null || typeof obj === 'undefined') {
    return;
  }

  // Force an array if not already something iterable
  if (typeof obj !== 'object') {
    /*eslint no-param-reassign:0*/
    obj = [obj];
  }

  if (isArray(obj)) {
    // Iterate over array values
    for (var i = 0, l = obj.length; i < l; i++) {
      fn.call(null, obj[i], i, obj);
    }
  } else {
    // Iterate over object keys
    for (var key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        fn.call(null, obj[key], key, obj);
      }
    }
  }
}

/**
 * Accepts varargs expecting each argument to be an object, then
 * immutably merges the properties of each object and returns result.
 *
 * When multiple objects contain the same key the later object in
 * the arguments list will take precedence.
 *
 * Example:
 *
 * ```js
 * var result = merge({foo: 123}, {foo: 456});
 * console.log(result.foo); // outputs 456
 * ```
 *
 * @param {Object} obj1 Object to merge
 * @returns {Object} Result of all merge properties
 */
function merge(/* obj1, obj2, obj3, ... */) {
  var result = {};
  function assignValue(val, key) {
    if (isPlainObject(result[key]) && isPlainObject(val)) {
      result[key] = merge(result[key], val);
    } else if (isPlainObject(val)) {
      result[key] = merge({}, val);
    } else if (isArray(val)) {
      result[key] = val.slice();
    } else {
      result[key] = val;
    }
  }

  for (var i = 0, l = arguments.length; i < l; i++) {
    forEach(arguments[i], assignValue);
  }
  return result;
}

/**
 * Extends object a by mutably adding to it the properties of object b.
 *
 * @param {Object} a The object to be extended
 * @param {Object} b The object to copy properties from
 * @param {Object} thisArg The object to bind function to
 * @return {Object} The resulting value of object a
 */
function extend(a, b, thisArg) {
  forEach(b, function assignValue(val, key) {
    if (thisArg && typeof val === 'function') {
      a[key] = bind(val, thisArg);
    } else {
      a[key] = val;
    }
  });
  return a;
}

/**
 * Remove byte order marker. This catches EF BB BF (the UTF-8 BOM)
 *
 * @param {string} content with BOM
 * @return {string} content value without BOM
 */
function stripBOM(content) {
  if (content.charCodeAt(0) === 0xFEFF) {
    content = content.slice(1);
  }
  return content;
}

var utils = {
  isArray: isArray,
  isArrayBuffer: isArrayBuffer,
  isBuffer: isBuffer,
  isFormData: isFormData,
  isArrayBufferView: isArrayBufferView,
  isString: isString,
  isNumber: isNumber,
  isObject: isObject,
  isPlainObject: isPlainObject,
  isUndefined: isUndefined,
  isDate: isDate,
  isFile: isFile,
  isBlob: isBlob,
  isFunction: isFunction,
  isStream: isStream,
  isURLSearchParams: isURLSearchParams,
  isStandardBrowserEnv: isStandardBrowserEnv,
  forEach: forEach,
  merge: merge,
  extend: extend,
  trim: trim,
  stripBOM: stripBOM
};

function encode(val) {
  return encodeURIComponent(val).
    replace(/%3A/gi, ':').
    replace(/%24/g, '$').
    replace(/%2C/gi, ',').
    replace(/%20/g, '+').
    replace(/%5B/gi, '[').
    replace(/%5D/gi, ']');
}

/**
 * Build a URL by appending params to the end
 *
 * @param {string} url The base of the url (e.g., http://www.google.com)
 * @param {object} [params] The params to be appended
 * @returns {string} The formatted url
 */
var buildURL = function buildURL(url, params, paramsSerializer) {
  /*eslint no-param-reassign:0*/
  if (!params) {
    return url;
  }

  var serializedParams;
  if (paramsSerializer) {
    serializedParams = paramsSerializer(params);
  } else if (utils.isURLSearchParams(params)) {
    serializedParams = params.toString();
  } else {
    var parts = [];

    utils.forEach(params, function serialize(val, key) {
      if (val === null || typeof val === 'undefined') {
        return;
      }

      if (utils.isArray(val)) {
        key = key + '[]';
      } else {
        val = [val];
      }

      utils.forEach(val, function parseValue(v) {
        if (utils.isDate(v)) {
          v = v.toISOString();
        } else if (utils.isObject(v)) {
          v = JSON.stringify(v);
        }
        parts.push(encode(key) + '=' + encode(v));
      });
    });

    serializedParams = parts.join('&');
  }

  if (serializedParams) {
    var hashmarkIndex = url.indexOf('#');
    if (hashmarkIndex !== -1) {
      url = url.slice(0, hashmarkIndex);
    }

    url += (url.indexOf('?') === -1 ? '?' : '&') + serializedParams;
  }

  return url;
};

function InterceptorManager() {
  this.handlers = [];
}

/**
 * Add a new interceptor to the stack
 *
 * @param {Function} fulfilled The function to handle `then` for a `Promise`
 * @param {Function} rejected The function to handle `reject` for a `Promise`
 *
 * @return {Number} An ID used to remove interceptor later
 */
InterceptorManager.prototype.use = function use(fulfilled, rejected, options) {
  this.handlers.push({
    fulfilled: fulfilled,
    rejected: rejected,
    synchronous: options ? options.synchronous : false,
    runWhen: options ? options.runWhen : null
  });
  return this.handlers.length - 1;
};

/**
 * Remove an interceptor from the stack
 *
 * @param {Number} id The ID that was returned by `use`
 */
InterceptorManager.prototype.eject = function eject(id) {
  if (this.handlers[id]) {
    this.handlers[id] = null;
  }
};

/**
 * Iterate over all the registered interceptors
 *
 * This method is particularly useful for skipping over any
 * interceptors that may have become `null` calling `eject`.
 *
 * @param {Function} fn The function to call for each interceptor
 */
InterceptorManager.prototype.forEach = function forEach(fn) {
  utils.forEach(this.handlers, function forEachHandler(h) {
    if (h !== null) {
      fn(h);
    }
  });
};

var InterceptorManager_1 = InterceptorManager;

var normalizeHeaderName = function normalizeHeaderName(headers, normalizedName) {
  utils.forEach(headers, function processHeader(value, name) {
    if (name !== normalizedName && name.toUpperCase() === normalizedName.toUpperCase()) {
      headers[normalizedName] = value;
      delete headers[name];
    }
  });
};

/**
 * Update an Error with the specified config, error code, and response.
 *
 * @param {Error} error The error to update.
 * @param {Object} config The config.
 * @param {string} [code] The error code (for example, 'ECONNABORTED').
 * @param {Object} [request] The request.
 * @param {Object} [response] The response.
 * @returns {Error} The error.
 */
var enhanceError = function enhanceError(error, config, code, request, response) {
  error.config = config;
  if (code) {
    error.code = code;
  }

  error.request = request;
  error.response = response;
  error.isAxiosError = true;

  error.toJSON = function toJSON() {
    return {
      // Standard
      message: this.message,
      name: this.name,
      // Microsoft
      description: this.description,
      number: this.number,
      // Mozilla
      fileName: this.fileName,
      lineNumber: this.lineNumber,
      columnNumber: this.columnNumber,
      stack: this.stack,
      // Axios
      config: this.config,
      code: this.code,
      status: this.response && this.response.status ? this.response.status : null
    };
  };
  return error;
};

var transitional = {
  silentJSONParsing: true,
  forcedJSONParsing: true,
  clarifyTimeoutError: false
};
transitional.silentJSONParsing;
transitional.forcedJSONParsing;
transitional.clarifyTimeoutError;

/**
 * Create an Error with the specified message, config, error code, request and response.
 *
 * @param {string} message The error message.
 * @param {Object} config The config.
 * @param {string} [code] The error code (for example, 'ECONNABORTED').
 * @param {Object} [request] The request.
 * @param {Object} [response] The response.
 * @returns {Error} The created error.
 */
var createError = function createError(message, config, code, request, response) {
  var error = new Error(message);
  return enhanceError(error, config, code, request, response);
};

/**
 * Resolve or reject a Promise based on response status.
 *
 * @param {Function} resolve A function that resolves the promise.
 * @param {Function} reject A function that rejects the promise.
 * @param {object} response The response.
 */
var settle = function settle(resolve, reject, response) {
  var validateStatus = response.config.validateStatus;
  if (!response.status || !validateStatus || validateStatus(response.status)) {
    resolve(response);
  } else {
    reject(createError(
      'Request failed with status code ' + response.status,
      response.config,
      null,
      response.request,
      response
    ));
  }
};

var cookies = (
  utils.isStandardBrowserEnv() ?

  // Standard browser envs support document.cookie
    (function standardBrowserEnv() {
      return {
        write: function write(name, value, expires, path, domain, secure) {
          var cookie = [];
          cookie.push(name + '=' + encodeURIComponent(value));

          if (utils.isNumber(expires)) {
            cookie.push('expires=' + new Date(expires).toGMTString());
          }

          if (utils.isString(path)) {
            cookie.push('path=' + path);
          }

          if (utils.isString(domain)) {
            cookie.push('domain=' + domain);
          }

          if (secure === true) {
            cookie.push('secure');
          }

          document.cookie = cookie.join('; ');
        },

        read: function read(name) {
          var match = document.cookie.match(new RegExp('(^|;\\s*)(' + name + ')=([^;]*)'));
          return (match ? decodeURIComponent(match[3]) : null);
        },

        remove: function remove(name) {
          this.write(name, '', Date.now() - 86400000);
        }
      };
    })() :

  // Non standard browser env (web workers, react-native) lack needed support.
    (function nonStandardBrowserEnv() {
      return {
        write: function write() {},
        read: function read() { return null; },
        remove: function remove() {}
      };
    })()
);

/**
 * Determines whether the specified URL is absolute
 *
 * @param {string} url The URL to test
 * @returns {boolean} True if the specified URL is absolute, otherwise false
 */
var isAbsoluteURL = function isAbsoluteURL(url) {
  // A URL is considered absolute if it begins with "<scheme>://" or "//" (protocol-relative URL).
  // RFC 3986 defines scheme name as a sequence of characters beginning with a letter and followed
  // by any combination of letters, digits, plus, period, or hyphen.
  return /^([a-z][a-z\d+\-.]*:)?\/\//i.test(url);
};

/**
 * Creates a new URL by combining the specified URLs
 *
 * @param {string} baseURL The base URL
 * @param {string} relativeURL The relative URL
 * @returns {string} The combined URL
 */
var combineURLs = function combineURLs(baseURL, relativeURL) {
  return relativeURL
    ? baseURL.replace(/\/+$/, '') + '/' + relativeURL.replace(/^\/+/, '')
    : baseURL;
};

/**
 * Creates a new URL by combining the baseURL with the requestedURL,
 * only when the requestedURL is not already an absolute URL.
 * If the requestURL is absolute, this function returns the requestedURL untouched.
 *
 * @param {string} baseURL The base URL
 * @param {string} requestedURL Absolute or relative URL to combine
 * @returns {string} The combined full path
 */
var buildFullPath = function buildFullPath(baseURL, requestedURL) {
  if (baseURL && !isAbsoluteURL(requestedURL)) {
    return combineURLs(baseURL, requestedURL);
  }
  return requestedURL;
};

// Headers whose duplicates are ignored by node
// c.f. https://nodejs.org/api/http.html#http_message_headers
var ignoreDuplicateOf = [
  'age', 'authorization', 'content-length', 'content-type', 'etag',
  'expires', 'from', 'host', 'if-modified-since', 'if-unmodified-since',
  'last-modified', 'location', 'max-forwards', 'proxy-authorization',
  'referer', 'retry-after', 'user-agent'
];

/**
 * Parse headers into an object
 *
 * ```
 * Date: Wed, 27 Aug 2014 08:58:49 GMT
 * Content-Type: application/json
 * Connection: keep-alive
 * Transfer-Encoding: chunked
 * ```
 *
 * @param {String} headers Headers needing to be parsed
 * @returns {Object} Headers parsed into an object
 */
var parseHeaders = function parseHeaders(headers) {
  var parsed = {};
  var key;
  var val;
  var i;

  if (!headers) { return parsed; }

  utils.forEach(headers.split('\n'), function parser(line) {
    i = line.indexOf(':');
    key = utils.trim(line.substr(0, i)).toLowerCase();
    val = utils.trim(line.substr(i + 1));

    if (key) {
      if (parsed[key] && ignoreDuplicateOf.indexOf(key) >= 0) {
        return;
      }
      if (key === 'set-cookie') {
        parsed[key] = (parsed[key] ? parsed[key] : []).concat([val]);
      } else {
        parsed[key] = parsed[key] ? parsed[key] + ', ' + val : val;
      }
    }
  });

  return parsed;
};

var isURLSameOrigin = (
  utils.isStandardBrowserEnv() ?

  // Standard browser envs have full support of the APIs needed to test
  // whether the request URL is of the same origin as current location.
    (function standardBrowserEnv() {
      var msie = /(msie|trident)/i.test(navigator.userAgent);
      var urlParsingNode = document.createElement('a');
      var originURL;

      /**
    * Parse a URL to discover it's components
    *
    * @param {String} url The URL to be parsed
    * @returns {Object}
    */
      function resolveURL(url) {
        var href = url;

        if (msie) {
        // IE needs attribute set twice to normalize properties
          urlParsingNode.setAttribute('href', href);
          href = urlParsingNode.href;
        }

        urlParsingNode.setAttribute('href', href);

        // urlParsingNode provides the UrlUtils interface - http://url.spec.whatwg.org/#urlutils
        return {
          href: urlParsingNode.href,
          protocol: urlParsingNode.protocol ? urlParsingNode.protocol.replace(/:$/, '') : '',
          host: urlParsingNode.host,
          search: urlParsingNode.search ? urlParsingNode.search.replace(/^\?/, '') : '',
          hash: urlParsingNode.hash ? urlParsingNode.hash.replace(/^#/, '') : '',
          hostname: urlParsingNode.hostname,
          port: urlParsingNode.port,
          pathname: (urlParsingNode.pathname.charAt(0) === '/') ?
            urlParsingNode.pathname :
            '/' + urlParsingNode.pathname
        };
      }

      originURL = resolveURL(window.location.href);

      /**
    * Determine if a URL shares the same origin as the current location
    *
    * @param {String} requestURL The URL to test
    * @returns {boolean} True if URL shares the same origin, otherwise false
    */
      return function isURLSameOrigin(requestURL) {
        var parsed = (utils.isString(requestURL)) ? resolveURL(requestURL) : requestURL;
        return (parsed.protocol === originURL.protocol &&
            parsed.host === originURL.host);
      };
    })() :

  // Non standard browser envs (web workers, react-native) lack needed support.
    (function nonStandardBrowserEnv() {
      return function isURLSameOrigin() {
        return true;
      };
    })()
);

/**
 * A `Cancel` is an object that is thrown when an operation is canceled.
 *
 * @class
 * @param {string=} message The message.
 */
function Cancel(message) {
  this.message = message;
}

Cancel.prototype.toString = function toString() {
  return 'Cancel' + (this.message ? ': ' + this.message : '');
};

Cancel.prototype.__CANCEL__ = true;

var Cancel_1 = Cancel;

var xhr = function xhrAdapter(config) {
  return new Promise(function dispatchXhrRequest(resolve, reject) {
    var requestData = config.data;
    var requestHeaders = config.headers;
    var responseType = config.responseType;
    var onCanceled;
    function done() {
      if (config.cancelToken) {
        config.cancelToken.unsubscribe(onCanceled);
      }

      if (config.signal) {
        config.signal.removeEventListener('abort', onCanceled);
      }
    }

    if (utils.isFormData(requestData)) {
      delete requestHeaders['Content-Type']; // Let the browser set it
    }

    var request = new XMLHttpRequest();

    // HTTP basic authentication
    if (config.auth) {
      var username = config.auth.username || '';
      var password = config.auth.password ? unescape(encodeURIComponent(config.auth.password)) : '';
      requestHeaders.Authorization = 'Basic ' + btoa(username + ':' + password);
    }

    var fullPath = buildFullPath(config.baseURL, config.url);
    request.open(config.method.toUpperCase(), buildURL(fullPath, config.params, config.paramsSerializer), true);

    // Set the request timeout in MS
    request.timeout = config.timeout;

    function onloadend() {
      if (!request) {
        return;
      }
      // Prepare the response
      var responseHeaders = 'getAllResponseHeaders' in request ? parseHeaders(request.getAllResponseHeaders()) : null;
      var responseData = !responseType || responseType === 'text' ||  responseType === 'json' ?
        request.responseText : request.response;
      var response = {
        data: responseData,
        status: request.status,
        statusText: request.statusText,
        headers: responseHeaders,
        config: config,
        request: request
      };

      settle(function _resolve(value) {
        resolve(value);
        done();
      }, function _reject(err) {
        reject(err);
        done();
      }, response);

      // Clean up request
      request = null;
    }

    if ('onloadend' in request) {
      // Use onloadend if available
      request.onloadend = onloadend;
    } else {
      // Listen for ready state to emulate onloadend
      request.onreadystatechange = function handleLoad() {
        if (!request || request.readyState !== 4) {
          return;
        }

        // The request errored out and we didn't get a response, this will be
        // handled by onerror instead
        // With one exception: request that using file: protocol, most browsers
        // will return status as 0 even though it's a successful request
        if (request.status === 0 && !(request.responseURL && request.responseURL.indexOf('file:') === 0)) {
          return;
        }
        // readystate handler is calling before onerror or ontimeout handlers,
        // so we should call onloadend on the next 'tick'
        setTimeout(onloadend);
      };
    }

    // Handle browser request cancellation (as opposed to a manual cancellation)
    request.onabort = function handleAbort() {
      if (!request) {
        return;
      }

      reject(createError('Request aborted', config, 'ECONNABORTED', request));

      // Clean up request
      request = null;
    };

    // Handle low level network errors
    request.onerror = function handleError() {
      // Real errors are hidden from us by the browser
      // onerror should only fire if it's a network error
      reject(createError('Network Error', config, null, request));

      // Clean up request
      request = null;
    };

    // Handle timeout
    request.ontimeout = function handleTimeout() {
      var timeoutErrorMessage = config.timeout ? 'timeout of ' + config.timeout + 'ms exceeded' : 'timeout exceeded';
      var transitional$1 = config.transitional || transitional;
      if (config.timeoutErrorMessage) {
        timeoutErrorMessage = config.timeoutErrorMessage;
      }
      reject(createError(
        timeoutErrorMessage,
        config,
        transitional$1.clarifyTimeoutError ? 'ETIMEDOUT' : 'ECONNABORTED',
        request));

      // Clean up request
      request = null;
    };

    // Add xsrf header
    // This is only done if running in a standard browser environment.
    // Specifically not if we're in a web worker, or react-native.
    if (utils.isStandardBrowserEnv()) {
      // Add xsrf header
      var xsrfValue = (config.withCredentials || isURLSameOrigin(fullPath)) && config.xsrfCookieName ?
        cookies.read(config.xsrfCookieName) :
        undefined;

      if (xsrfValue) {
        requestHeaders[config.xsrfHeaderName] = xsrfValue;
      }
    }

    // Add headers to the request
    if ('setRequestHeader' in request) {
      utils.forEach(requestHeaders, function setRequestHeader(val, key) {
        if (typeof requestData === 'undefined' && key.toLowerCase() === 'content-type') {
          // Remove Content-Type if data is undefined
          delete requestHeaders[key];
        } else {
          // Otherwise add header to the request
          request.setRequestHeader(key, val);
        }
      });
    }

    // Add withCredentials to request if needed
    if (!utils.isUndefined(config.withCredentials)) {
      request.withCredentials = !!config.withCredentials;
    }

    // Add responseType to request if needed
    if (responseType && responseType !== 'json') {
      request.responseType = config.responseType;
    }

    // Handle progress if needed
    if (typeof config.onDownloadProgress === 'function') {
      request.addEventListener('progress', config.onDownloadProgress);
    }

    // Not all browsers support upload events
    if (typeof config.onUploadProgress === 'function' && request.upload) {
      request.upload.addEventListener('progress', config.onUploadProgress);
    }

    if (config.cancelToken || config.signal) {
      // Handle cancellation
      // eslint-disable-next-line func-names
      onCanceled = function(cancel) {
        if (!request) {
          return;
        }
        reject(!cancel || (cancel && cancel.type) ? new Cancel_1('canceled') : cancel);
        request.abort();
        request = null;
      };

      config.cancelToken && config.cancelToken.subscribe(onCanceled);
      if (config.signal) {
        config.signal.aborted ? onCanceled() : config.signal.addEventListener('abort', onCanceled);
      }
    }

    if (!requestData) {
      requestData = null;
    }

    // Send the request
    request.send(requestData);
  });
};

var DEFAULT_CONTENT_TYPE = {
  'Content-Type': 'application/x-www-form-urlencoded'
};

function setContentTypeIfUnset(headers, value) {
  if (!utils.isUndefined(headers) && utils.isUndefined(headers['Content-Type'])) {
    headers['Content-Type'] = value;
  }
}

function getDefaultAdapter() {
  var adapter;
  if (typeof XMLHttpRequest !== 'undefined') {
    // For browsers use XHR adapter
    adapter = xhr;
  } else if (typeof process !== 'undefined' && Object.prototype.toString.call(process) === '[object process]') {
    // For node use HTTP adapter
    adapter = xhr;
  }
  return adapter;
}

function stringifySafely(rawValue, parser, encoder) {
  if (utils.isString(rawValue)) {
    try {
      (parser || JSON.parse)(rawValue);
      return utils.trim(rawValue);
    } catch (e) {
      if (e.name !== 'SyntaxError') {
        throw e;
      }
    }
  }

  return (encoder || JSON.stringify)(rawValue);
}

var defaults = {

  transitional: transitional,

  adapter: getDefaultAdapter(),

  transformRequest: [function transformRequest(data, headers) {
    normalizeHeaderName(headers, 'Accept');
    normalizeHeaderName(headers, 'Content-Type');

    if (utils.isFormData(data) ||
      utils.isArrayBuffer(data) ||
      utils.isBuffer(data) ||
      utils.isStream(data) ||
      utils.isFile(data) ||
      utils.isBlob(data)
    ) {
      return data;
    }
    if (utils.isArrayBufferView(data)) {
      return data.buffer;
    }
    if (utils.isURLSearchParams(data)) {
      setContentTypeIfUnset(headers, 'application/x-www-form-urlencoded;charset=utf-8');
      return data.toString();
    }
    if (utils.isObject(data) || (headers && headers['Content-Type'] === 'application/json')) {
      setContentTypeIfUnset(headers, 'application/json');
      return stringifySafely(data);
    }
    return data;
  }],

  transformResponse: [function transformResponse(data) {
    var transitional = this.transitional || defaults.transitional;
    var silentJSONParsing = transitional && transitional.silentJSONParsing;
    var forcedJSONParsing = transitional && transitional.forcedJSONParsing;
    var strictJSONParsing = !silentJSONParsing && this.responseType === 'json';

    if (strictJSONParsing || (forcedJSONParsing && utils.isString(data) && data.length)) {
      try {
        return JSON.parse(data);
      } catch (e) {
        if (strictJSONParsing) {
          if (e.name === 'SyntaxError') {
            throw enhanceError(e, this, 'E_JSON_PARSE');
          }
          throw e;
        }
      }
    }

    return data;
  }],

  /**
   * A timeout in milliseconds to abort a request. If set to 0 (default) a
   * timeout is not created.
   */
  timeout: 0,

  xsrfCookieName: 'XSRF-TOKEN',
  xsrfHeaderName: 'X-XSRF-TOKEN',

  maxContentLength: -1,
  maxBodyLength: -1,

  validateStatus: function validateStatus(status) {
    return status >= 200 && status < 300;
  },

  headers: {
    common: {
      'Accept': 'application/json, text/plain, */*'
    }
  }
};

utils.forEach(['delete', 'get', 'head'], function forEachMethodNoData(method) {
  defaults.headers[method] = {};
});

utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
  defaults.headers[method] = utils.merge(DEFAULT_CONTENT_TYPE);
});

var defaults_1 = defaults;

/**
 * Transform the data for a request or a response
 *
 * @param {Object|String} data The data to be transformed
 * @param {Array} headers The headers for the request or response
 * @param {Array|Function} fns A single function or Array of functions
 * @returns {*} The resulting transformed data
 */
var transformData = function transformData(data, headers, fns) {
  var context = this || defaults_1;
  /*eslint no-param-reassign:0*/
  utils.forEach(fns, function transform(fn) {
    data = fn.call(context, data, headers);
  });

  return data;
};

var isCancel = function isCancel(value) {
  return !!(value && value.__CANCEL__);
};

/**
 * Throws a `Cancel` if cancellation has been requested.
 */
function throwIfCancellationRequested(config) {
  if (config.cancelToken) {
    config.cancelToken.throwIfRequested();
  }

  if (config.signal && config.signal.aborted) {
    throw new Cancel_1('canceled');
  }
}

/**
 * Dispatch a request to the server using the configured adapter.
 *
 * @param {object} config The config that is to be used for the request
 * @returns {Promise} The Promise to be fulfilled
 */
var dispatchRequest = function dispatchRequest(config) {
  throwIfCancellationRequested(config);

  // Ensure headers exist
  config.headers = config.headers || {};

  // Transform request data
  config.data = transformData.call(
    config,
    config.data,
    config.headers,
    config.transformRequest
  );

  // Flatten headers
  config.headers = utils.merge(
    config.headers.common || {},
    config.headers[config.method] || {},
    config.headers
  );

  utils.forEach(
    ['delete', 'get', 'head', 'post', 'put', 'patch', 'common'],
    function cleanHeaderConfig(method) {
      delete config.headers[method];
    }
  );

  var adapter = config.adapter || defaults_1.adapter;

  return adapter(config).then(function onAdapterResolution(response) {
    throwIfCancellationRequested(config);

    // Transform response data
    response.data = transformData.call(
      config,
      response.data,
      response.headers,
      config.transformResponse
    );

    return response;
  }, function onAdapterRejection(reason) {
    if (!isCancel(reason)) {
      throwIfCancellationRequested(config);

      // Transform response data
      if (reason && reason.response) {
        reason.response.data = transformData.call(
          config,
          reason.response.data,
          reason.response.headers,
          config.transformResponse
        );
      }
    }

    return Promise.reject(reason);
  });
};

/**
 * Config-specific merge-function which creates a new config-object
 * by merging two configuration objects together.
 *
 * @param {Object} config1
 * @param {Object} config2
 * @returns {Object} New object resulting from merging config2 to config1
 */
var mergeConfig = function mergeConfig(config1, config2) {
  // eslint-disable-next-line no-param-reassign
  config2 = config2 || {};
  var config = {};

  function getMergedValue(target, source) {
    if (utils.isPlainObject(target) && utils.isPlainObject(source)) {
      return utils.merge(target, source);
    } else if (utils.isPlainObject(source)) {
      return utils.merge({}, source);
    } else if (utils.isArray(source)) {
      return source.slice();
    }
    return source;
  }

  // eslint-disable-next-line consistent-return
  function mergeDeepProperties(prop) {
    if (!utils.isUndefined(config2[prop])) {
      return getMergedValue(config1[prop], config2[prop]);
    } else if (!utils.isUndefined(config1[prop])) {
      return getMergedValue(undefined, config1[prop]);
    }
  }

  // eslint-disable-next-line consistent-return
  function valueFromConfig2(prop) {
    if (!utils.isUndefined(config2[prop])) {
      return getMergedValue(undefined, config2[prop]);
    }
  }

  // eslint-disable-next-line consistent-return
  function defaultToConfig2(prop) {
    if (!utils.isUndefined(config2[prop])) {
      return getMergedValue(undefined, config2[prop]);
    } else if (!utils.isUndefined(config1[prop])) {
      return getMergedValue(undefined, config1[prop]);
    }
  }

  // eslint-disable-next-line consistent-return
  function mergeDirectKeys(prop) {
    if (prop in config2) {
      return getMergedValue(config1[prop], config2[prop]);
    } else if (prop in config1) {
      return getMergedValue(undefined, config1[prop]);
    }
  }

  var mergeMap = {
    'url': valueFromConfig2,
    'method': valueFromConfig2,
    'data': valueFromConfig2,
    'baseURL': defaultToConfig2,
    'transformRequest': defaultToConfig2,
    'transformResponse': defaultToConfig2,
    'paramsSerializer': defaultToConfig2,
    'timeout': defaultToConfig2,
    'timeoutMessage': defaultToConfig2,
    'withCredentials': defaultToConfig2,
    'adapter': defaultToConfig2,
    'responseType': defaultToConfig2,
    'xsrfCookieName': defaultToConfig2,
    'xsrfHeaderName': defaultToConfig2,
    'onUploadProgress': defaultToConfig2,
    'onDownloadProgress': defaultToConfig2,
    'decompress': defaultToConfig2,
    'maxContentLength': defaultToConfig2,
    'maxBodyLength': defaultToConfig2,
    'transport': defaultToConfig2,
    'httpAgent': defaultToConfig2,
    'httpsAgent': defaultToConfig2,
    'cancelToken': defaultToConfig2,
    'socketPath': defaultToConfig2,
    'responseEncoding': defaultToConfig2,
    'validateStatus': mergeDirectKeys
  };

  utils.forEach(Object.keys(config1).concat(Object.keys(config2)), function computeConfigValue(prop) {
    var merge = mergeMap[prop] || mergeDeepProperties;
    var configValue = merge(prop);
    (utils.isUndefined(configValue) && merge !== mergeDirectKeys) || (config[prop] = configValue);
  });

  return config;
};

var data = {
  "version": "0.26.1"
};

var VERSION = data.version;

var validators$1 = {};

// eslint-disable-next-line func-names
['object', 'boolean', 'number', 'function', 'string', 'symbol'].forEach(function(type, i) {
  validators$1[type] = function validator(thing) {
    return typeof thing === type || 'a' + (i < 1 ? 'n ' : ' ') + type;
  };
});

var deprecatedWarnings = {};

/**
 * Transitional option validator
 * @param {function|boolean?} validator - set to false if the transitional option has been removed
 * @param {string?} version - deprecated version / removed since version
 * @param {string?} message - some message with additional info
 * @returns {function}
 */
validators$1.transitional = function transitional(validator, version, message) {
  function formatMessage(opt, desc) {
    return '[Axios v' + VERSION + '] Transitional option \'' + opt + '\'' + desc + (message ? '. ' + message : '');
  }

  // eslint-disable-next-line func-names
  return function(value, opt, opts) {
    if (validator === false) {
      throw new Error(formatMessage(opt, ' has been removed' + (version ? ' in ' + version : '')));
    }

    if (version && !deprecatedWarnings[opt]) {
      deprecatedWarnings[opt] = true;
      // eslint-disable-next-line no-console
      console.warn(
        formatMessage(
          opt,
          ' has been deprecated since v' + version + ' and will be removed in the near future'
        )
      );
    }

    return validator ? validator(value, opt, opts) : true;
  };
};

/**
 * Assert object's properties type
 * @param {object} options
 * @param {object} schema
 * @param {boolean?} allowUnknown
 */

function assertOptions(options, schema, allowUnknown) {
  if (typeof options !== 'object') {
    throw new TypeError('options must be an object');
  }
  var keys = Object.keys(options);
  var i = keys.length;
  while (i-- > 0) {
    var opt = keys[i];
    var validator = schema[opt];
    if (validator) {
      var value = options[opt];
      var result = value === undefined || validator(value, opt, options);
      if (result !== true) {
        throw new TypeError('option ' + opt + ' must be ' + result);
      }
      continue;
    }
    if (allowUnknown !== true) {
      throw Error('Unknown option ' + opt);
    }
  }
}

var validator = {
  assertOptions: assertOptions,
  validators: validators$1
};

var validators = validator.validators;
/**
 * Create a new instance of Axios
 *
 * @param {Object} instanceConfig The default config for the instance
 */
function Axios(instanceConfig) {
  this.defaults = instanceConfig;
  this.interceptors = {
    request: new InterceptorManager_1(),
    response: new InterceptorManager_1()
  };
}

/**
 * Dispatch a request
 *
 * @param {Object} config The config specific for this request (merged with this.defaults)
 */
Axios.prototype.request = function request(configOrUrl, config) {
  /*eslint no-param-reassign:0*/
  // Allow for axios('example/url'[, config]) a la fetch API
  if (typeof configOrUrl === 'string') {
    config = config || {};
    config.url = configOrUrl;
  } else {
    config = configOrUrl || {};
  }

  config = mergeConfig(this.defaults, config);

  // Set config.method
  if (config.method) {
    config.method = config.method.toLowerCase();
  } else if (this.defaults.method) {
    config.method = this.defaults.method.toLowerCase();
  } else {
    config.method = 'get';
  }

  var transitional = config.transitional;

  if (transitional !== undefined) {
    validator.assertOptions(transitional, {
      silentJSONParsing: validators.transitional(validators.boolean),
      forcedJSONParsing: validators.transitional(validators.boolean),
      clarifyTimeoutError: validators.transitional(validators.boolean)
    }, false);
  }

  // filter out skipped interceptors
  var requestInterceptorChain = [];
  var synchronousRequestInterceptors = true;
  this.interceptors.request.forEach(function unshiftRequestInterceptors(interceptor) {
    if (typeof interceptor.runWhen === 'function' && interceptor.runWhen(config) === false) {
      return;
    }

    synchronousRequestInterceptors = synchronousRequestInterceptors && interceptor.synchronous;

    requestInterceptorChain.unshift(interceptor.fulfilled, interceptor.rejected);
  });

  var responseInterceptorChain = [];
  this.interceptors.response.forEach(function pushResponseInterceptors(interceptor) {
    responseInterceptorChain.push(interceptor.fulfilled, interceptor.rejected);
  });

  var promise;

  if (!synchronousRequestInterceptors) {
    var chain = [dispatchRequest, undefined];

    Array.prototype.unshift.apply(chain, requestInterceptorChain);
    chain = chain.concat(responseInterceptorChain);

    promise = Promise.resolve(config);
    while (chain.length) {
      promise = promise.then(chain.shift(), chain.shift());
    }

    return promise;
  }


  var newConfig = config;
  while (requestInterceptorChain.length) {
    var onFulfilled = requestInterceptorChain.shift();
    var onRejected = requestInterceptorChain.shift();
    try {
      newConfig = onFulfilled(newConfig);
    } catch (error) {
      onRejected(error);
      break;
    }
  }

  try {
    promise = dispatchRequest(newConfig);
  } catch (error) {
    return Promise.reject(error);
  }

  while (responseInterceptorChain.length) {
    promise = promise.then(responseInterceptorChain.shift(), responseInterceptorChain.shift());
  }

  return promise;
};

Axios.prototype.getUri = function getUri(config) {
  config = mergeConfig(this.defaults, config);
  return buildURL(config.url, config.params, config.paramsSerializer).replace(/^\?/, '');
};

// Provide aliases for supported request methods
utils.forEach(['delete', 'get', 'head', 'options'], function forEachMethodNoData(method) {
  /*eslint func-names:0*/
  Axios.prototype[method] = function(url, config) {
    return this.request(mergeConfig(config || {}, {
      method: method,
      url: url,
      data: (config || {}).data
    }));
  };
});

utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
  /*eslint func-names:0*/
  Axios.prototype[method] = function(url, data, config) {
    return this.request(mergeConfig(config || {}, {
      method: method,
      url: url,
      data: data
    }));
  };
});

var Axios_1 = Axios;

/**
 * A `CancelToken` is an object that can be used to request cancellation of an operation.
 *
 * @class
 * @param {Function} executor The executor function.
 */
function CancelToken(executor) {
  if (typeof executor !== 'function') {
    throw new TypeError('executor must be a function.');
  }

  var resolvePromise;

  this.promise = new Promise(function promiseExecutor(resolve) {
    resolvePromise = resolve;
  });

  var token = this;

  // eslint-disable-next-line func-names
  this.promise.then(function(cancel) {
    if (!token._listeners) return;

    var i;
    var l = token._listeners.length;

    for (i = 0; i < l; i++) {
      token._listeners[i](cancel);
    }
    token._listeners = null;
  });

  // eslint-disable-next-line func-names
  this.promise.then = function(onfulfilled) {
    var _resolve;
    // eslint-disable-next-line func-names
    var promise = new Promise(function(resolve) {
      token.subscribe(resolve);
      _resolve = resolve;
    }).then(onfulfilled);

    promise.cancel = function reject() {
      token.unsubscribe(_resolve);
    };

    return promise;
  };

  executor(function cancel(message) {
    if (token.reason) {
      // Cancellation has already been requested
      return;
    }

    token.reason = new Cancel_1(message);
    resolvePromise(token.reason);
  });
}

/**
 * Throws a `Cancel` if cancellation has been requested.
 */
CancelToken.prototype.throwIfRequested = function throwIfRequested() {
  if (this.reason) {
    throw this.reason;
  }
};

/**
 * Subscribe to the cancel signal
 */

CancelToken.prototype.subscribe = function subscribe(listener) {
  if (this.reason) {
    listener(this.reason);
    return;
  }

  if (this._listeners) {
    this._listeners.push(listener);
  } else {
    this._listeners = [listener];
  }
};

/**
 * Unsubscribe from the cancel signal
 */

CancelToken.prototype.unsubscribe = function unsubscribe(listener) {
  if (!this._listeners) {
    return;
  }
  var index = this._listeners.indexOf(listener);
  if (index !== -1) {
    this._listeners.splice(index, 1);
  }
};

/**
 * Returns an object that contains a new `CancelToken` and a function that, when called,
 * cancels the `CancelToken`.
 */
CancelToken.source = function source() {
  var cancel;
  var token = new CancelToken(function executor(c) {
    cancel = c;
  });
  return {
    token: token,
    cancel: cancel
  };
};

var CancelToken_1 = CancelToken;

/**
 * Syntactic sugar for invoking a function and expanding an array for arguments.
 *
 * Common use case would be to use `Function.prototype.apply`.
 *
 *  ```js
 *  function f(x, y, z) {}
 *  var args = [1, 2, 3];
 *  f.apply(null, args);
 *  ```
 *
 * With `spread` this example can be re-written.
 *
 *  ```js
 *  spread(function(x, y, z) {})([1, 2, 3]);
 *  ```
 *
 * @param {Function} callback
 * @returns {Function}
 */
var spread = function spread(callback) {
  return function wrap(arr) {
    return callback.apply(null, arr);
  };
};

/**
 * Determines whether the payload is an error thrown by Axios
 *
 * @param {*} payload The value to test
 * @returns {boolean} True if the payload is an error thrown by Axios, otherwise false
 */
var isAxiosError = function isAxiosError(payload) {
  return utils.isObject(payload) && (payload.isAxiosError === true);
};

/**
 * Create an instance of Axios
 *
 * @param {Object} defaultConfig The default config for the instance
 * @return {Axios} A new instance of Axios
 */
function createInstance(defaultConfig) {
  var context = new Axios_1(defaultConfig);
  var instance = bind(Axios_1.prototype.request, context);

  // Copy axios.prototype to instance
  utils.extend(instance, Axios_1.prototype, context);

  // Copy context to instance
  utils.extend(instance, context);

  // Factory for creating new instances
  instance.create = function create(instanceConfig) {
    return createInstance(mergeConfig(defaultConfig, instanceConfig));
  };

  return instance;
}

// Create the default instance to be exported
var axios$1 = createInstance(defaults_1);

// Expose Axios class to allow class inheritance
axios$1.Axios = Axios_1;

// Expose Cancel & CancelToken
axios$1.Cancel = Cancel_1;
axios$1.CancelToken = CancelToken_1;
axios$1.isCancel = isCancel;
axios$1.VERSION = data.version;

// Expose all/spread
axios$1.all = function all(promises) {
  return Promise.all(promises);
};
axios$1.spread = spread;

// Expose isAxiosError
axios$1.isAxiosError = isAxiosError;

var axios_1 = axios$1;

// Allow use of default import syntax in TypeScript
var default_1 = axios$1;
axios_1.default = default_1;

var axios = axios_1;

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

var percentage = function percentage(loaded, total) {
  return Math.floor(loaded * 1.0) / total;
};

var FetchBar = /*#__PURE__*/function (_ProgressBarImpl) {
  _inherits(FetchBar, _ProgressBarImpl);

  var _super = _createSuper(FetchBar);

  function FetchBar(option) {
    _classCallCheck(this, FetchBar);

    return _super.call(this, option);
  } // axios 요청, 응답을 과정에 프로그레스 바를 설정


  _createClass(FetchBar, [{
    key: "progress",
    value: function progress() {
      var _this = this;

      var instance = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : axios;
      var requestsCounter = 0; // 요청 인터셉터 추가, 프로그레스 시작 시 바 설정

      var setupStartProgress = function setupStartProgress() {
        instance.interceptors.request.use(function (config) {
          requestsCounter++;

          _this.start();

          return config;
        });
      }; // progressEvent 시 프로그레스 바 실행


      var setupUpdateProgress = function setupUpdateProgress() {
        var update = function update(e) {
          return _this.set(percentage(e.loaded, e.total) * 100);
        };

        instance.defaults.onDownloadProgress = update;
        instance.defaults.onUploadProgress = update;
        console.log(_this.progressBar.style.width);
      }; // 프로그레스가 끝날 시 프로그레스 바 종료


      var setupStopProgress = function setupStopProgress() {
        var responseFunc = function responseFunc(response) {
          if (--requestsCounter === 0) {
            _this.done();
          }
        }; // 오류 응답 처리


        var errorFunc = function errorFunc(error) {
          if (--requestsCounter === 0) {
            _this.hide();
          }

          return Promise.reject(error);
        };

        instance.interceptors.response.use(responseFunc, errorFunc);
      };

      setupStartProgress();
      setupUpdateProgress();
      setupStopProgress();
    }
  }, {
    key: "setup",
    value: function setup() {
      _get(_getPrototypeOf(FetchBar.prototype), "setup", this).call(this);
    }
  }]);

  return FetchBar;
}(ProgressBarImpl);

exports.FetchBar = FetchBar;
exports.LoadingBar = LoadingBar;
exports.ProgressBar = ProgressBarImpl;
exports.ScrollBar = ScrollBar;
