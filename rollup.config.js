import alias from '@rollup/plugin-alias'
import resolve from '@rollup/plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'
import babel from '@rollup/plugin-babel'
import pkg from './package.json'
import image from '@rollup/plugin-image'
import copy from 'rollup-plugin-copy'
import css from 'rollup-plugin-import-css'
import path from 'path'

const extensions = ['.js', '.ts', 'jsx', 'tsx', 'html']
const name = pkg.rollupName
const projectRoot = path.resolve(__dirname, './')
const globals = { axios: 'axios' }

export default {
	input: './src/index.ts',

	plugins: [
		css(),
		image({
			extensions: /\.(png|jpg|jpeg|gif|svg)$/,
			limit: 10000,
		}),
		resolve({ extensions }),
		commonjs({ include: ['node_modules/axios/**'] }),
		babel({
			exclude: 'node_modules/**',
			include: ['src/**/*', 'node_modules/axios'],
			extensions,
			babelHelpers: 'bundled',
			presets: [
				[
					'@babel/preset-env',
					{
						targets: '>0.2%, not dead, not op_mini all',
					},
				],
				'@babel/preset-typescript',
			],
			plugins: ['@babel/plugin-transform-runtime'],

			babelHelpers: 'runtime',
		}),

		alias({
			entries: [
				{
					find: '@',
					replacement: `${path.resolve(projectRoot, 'src')}`,
				},
			],
		}),
		copy({
			targets: [{ src: 'src/public', dest: 'dist' }],
		}),
	],

	output: [
		{ file: pkg.cjs, name, format: 'cjs', globals },
		{ file: pkg.esm, name, format: 'esm', globals },
	],
}
